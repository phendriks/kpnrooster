__author__ = 'Dana Kool'

import requests
import uuid
from datetime import datetime, timedelta
from dateutil.parser import parse

class LoginError(Exception):
    pass


class Rooster(object):
    def __init__(self, user, passwd, simple):
        """
        Contructor krijgt parameters mee en voert meteen de gehele functie uit.
        in self.ical komt dan de iCal String te staan.
        """
        self.ical = self.makeRequest(user, passwd, simple)

    def __repr__(self):
        """
        Als je het object zou printen retourneert deze ook de iCal inhoud als String.
        """
        return self.ical


    def makeRequest(self, user, passwd, simple):

        BASEURLTOKEN = "https://wfo.kpn.com/wfo/rest/core-api/auth/token"
        CONTACTINFO = "https://wfo.kpn.com/wfo/rest/core-api/v2/emp/get"
        ORGIDURL = "https://wfo.kpn.com/wfo/rest/core-api/org/get"
        APIURL = "https://wfo.kpn.com/wfo/rest/fs-api/schedule/get"

        skip_simple = ["Onbeschikbaarheid FlexManager", "Verlof"]
        skip_complex = ["Onbeschikbaarheid FlexManager"]

        s = requests.Session()

        s.headers.update({'User-agent': 'Dalvik/2.1.0 (Linux; U; Android 5.1.2; SM-G900F Build/LMY48Y)'})

        tokenr = s.request("POST", BASEURLTOKEN, json={"password": passwd, "user": user})
        token = tokenr.json()

        s.headers.update({'Impact360AuthToken': token["AuthToken"]["token"]})
        contactParams = {"fields":"userInfo,contactInfo","includeUser":"true"}
        s.cookies.clear()
        contactr = s.request("GET", CONTACTINFO, params=contactParams)
        contact = contactr.json()
        ############
        id = contact["employee"][0]["id"]
        voornaam = contact["employee"][0]["userInfo"]["firstName"]
        achternaam = contact["employee"][0]["userInfo"]["lastName"]
        ############
        date_start = datetime.today() - timedelta(weeks=2)
        date_end = datetime.today() + timedelta(weeks=4)
        ############
        beginDatum = date_start.strftime("%Y-%m-%dT23:00:00Z")
        eindDatum = date_end.strftime("%Y-%m-%dT23:00:00Z")
        orgPostData = {"includeUser": "true", "includeParentOrg": "false"}
        orgR = s.request("GET", ORGIDURL, params=orgPostData)
        ############
        org = orgR.json()
        orgId = org["organization"][0]["id"]

        postdata = {"employeeId": id,
                    "organizationId": orgId,
                    "startTime": beginDatum,
                    "endTime": eindDatum
                    }

        feedr = s.request("GET", APIURL, params=postdata)
        feed = feedr.json()

        s.close()



        if simple is True:
            detail = " (simpel)"
        if simple is False:
            detail = " (gedetailleerd)"

        ###################
        cal = 'BEGIN:VCALENDAR\r\n'
        cal += 'VERSION:2.0\r\n'
        cal += 'PRODID:-//Rooster door Dana Kool //rooster.kpn.com//\r\n'
        cal += 'METHOD:PUBLISH\r\n'
        cal += 'CALSCALE:GREGORIAN\r\n'
        cal += 'X-WR-TIMEZONE:Europe/Amsterdam\r\n'
        cal += 'X-WR-CALNAME:KPN Rooster voor ' + voornaam + ' ' + achternaam + detail + '\r\n'
        cal += 'X-WR-CALDESC:KPN Rooster\r\n'
        cal += 'REFRESH-INTERVAL;VALUE=DURATION:PT12H\r\n'
        cal += 'X-PUBLISHED-TTL:PT12H\r\n'
        cal += 'BEGIN:VTIMEZONE\r\n'
        cal += 'TZID:Europe/Amsterdam\r\n'
        cal += 'TZURL:http://tzurl.org/zoneinfo-outlook/Europe/Amsterdam\r\n'
        cal += 'X-LIC-LOCATION:Europe/Amsterdam\r\n'
        cal += 'BEGIN:DAYLIGHT\r\n'
        cal += 'TZOFFSETFROM:+0100\r\n'
        cal += 'TZOFFSETTO:+0200\r\n'
        cal += 'TZNAME:CEST\r\n'
        cal += 'DTSTART:19700329T020000\r\n'
        cal += 'RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\r\n'
        cal += 'END:DAYLIGHT\r\n'
        cal += 'BEGIN:STANDARD\r\n'
        cal += 'TZOFFSETFROM:+0200\r\n'
        cal += 'TZOFFSETTO:+0100\r\n'
        cal += 'TZNAME:CET\r\n'
        cal += 'DTSTART:19701025T030000\r\n'
        cal += 'RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\r\n'
        cal += 'END:STANDARD\r\n'
        cal += 'END:VTIMEZONE\r\n'

        datenow = datetime.now()
        now = datenow.strftime("%Y%m%dT%H%M%SZ")
        ##############

        if simple is True:
            dagen = {}
            for x in feed["scheduleEvents"]:
                if not x["activityName"] in skip_simple:
                    dag = parse(x["startTime"]).strftime("%Y%m%d")
                    if dag not in dagen:
                        dagen[dag] = {"start": parse(x["startTime"]).timestamp(), "end": parse(x["endTime"]).timestamp()}
                    else:

                        if parse(x["startTime"]).timestamp() < dagen[dag]["start"]:
                            dagen[dag]["start"] = parse(x["startTime"]).timestamp()

                        if parse(x["endTime"]).timestamp() > dagen[dag]["end"]:
                            dagen[dag]["end"] = parse(x["endTime"]).timestamp()

            for lel in dagen:
                cBeginTijd = datetime.fromtimestamp(dagen[lel]["start"])
                cEindTijd = datetime.fromtimestamp(dagen[lel]["end"])
                cal += 'BEGIN:VEVENT\r\n'
                cal += 'DTSTAMP:' + now + '\r\n'
                cal += 'DTSTART;TZID=Europe/Amsterdam:' + cBeginTijd.strftime("%Y%m%dT%H%M%S") + '\r\n'
                cal += 'DTEND;TZID=Europe/Amsterdam:' + cEindTijd.strftime("%Y%m%dT%H%M%S") + '\r\n'
                cal += 'SUMMARY:' + "Werk" + '\r\n'
                cal += 'LOCATION:\r\n'
                cal += 'TRANSP:OPAQUE\r\n'
                cal += 'STATUS:CONFIRMED\r\n'
                cal += 'SEQUENCE:0\r\n'
                cal += 'CREATED:' + now + '\r\n'
                cal += 'LAST-MODIFIED:' + now + '\r\n'
                cal += 'UID:' + str(uuid.uuid4()) + '\r\n'
                cal += 'DESCRIPTION:\r\n'
                cal += 'END:VEVENT\r\n'

        else:
            for x in feed["scheduleEvents"]:
                if not x["activityName"] in skip_complex:
                    cBeginTijd = datetime.fromtimestamp(parse(x["startTime"]).timestamp())
                    cEindTijd = datetime.fromtimestamp(parse(x["endTime"]).timestamp())
                    cal += 'BEGIN:VEVENT\r\n'
                    cal += 'DTSTAMP:' + now + '\r\n'
                    cal += 'DTSTART;TZID=Europe/Amsterdam:' + cBeginTijd.strftime("%Y%m%dT%H%M%S") + '\r\n'
                    cal += 'DTEND;TZID=Europe/Amsterdam:' + cEindTijd.strftime("%Y%m%dT%H%M%S") + '\r\n'
                    cal += 'SUMMARY:' + x["activityName"] + '\r\n'
                    cal += 'LOCATION:\r\n'
                    cal += 'TRANSP:OPAQUE\r\n'
                    cal += 'STATUS:CONFIRMED\r\n'
                    cal += 'SEQUENCE:0\r\n'
                    cal += 'CREATED:' + now + '\r\n'
                    cal += 'LAST-MODIFIED:' + now + '\r\n'
                    cal += 'UID:' + str(uuid.uuid4()) + '\r\n'
                    cal += 'DESCRIPTION:\r\n'
                    cal += 'END:VEVENT\r\n'

        cal += 'END:VCALENDAR\r\n'

        return cal
