<?php
include("encryption.php");

date_default_timezone_set('UTC');

class Web {
	private $_curl;
	public $byDay = false;

	public function __construct () {
		$this->_curl = curl_init();
	}

	public function getToken($username, $password) {
		$data = array(
			'user' => $username,
			'password' => $password
		);

		$dataJson = json_encode($data);

		curl_setopt($this->_curl, CURLOPT_URL, "https://wfo.kpn.com/wfo/rest/core-api/auth/token");
		curl_setopt($this->_curl, CURLOPT_USERAGENT, 'Dalvik/2.1.0 (Linux; U; Android 5.1.1; SM-G900F Build/LMY48Y)');
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->_curl, CURLOPT_POST, true);
		curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $dataJson);
		curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
			'Content-Length: ' . strlen($dataJson))
		);
		$output = curl_exec($this->_curl);

		if($jsonDecode = json_decode($output, true))
			if(isset($jsonDecode['AuthToken']['token']))
				return $jsonDecode['AuthToken']['token'];

		return false;
	}

	public function getUserInfo($token) {
		$this->renew();

		$data = array(
			'fields' => 'userInfo,contactInfo',
			'includeUser' => 'true'
		);
		curl_setopt($this->_curl, CURLOPT_URL, "https://wfo.kpn.com/wfo/rest/core-api/v2/emp/get?" . http_build_query($data));
		curl_setopt($this->_curl, CURLOPT_USERAGENT, 'Dalvik/2.1.0 (Linux; U; Android 5.1.1; SM-G900F Build/LMY48Y)');
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array(
				'Impact360AuthToken: ' . $token
		));

		$output = curl_exec($this->_curl);
		if($jsonDecode = json_decode($output, true))
			return $jsonDecode;
		return false;
	}

	public function getOrganisationId($token) {
		$this->renew();

		$data = array(
			'includeParentOrg' => 'false',
			'includeUser' => 'true'
		);
		curl_setopt($this->_curl, CURLOPT_URL, "https://wfo.kpn.com/wfo/rest/core-api/org/get?" . http_build_query($data));
		curl_setopt($this->_curl, CURLOPT_USERAGENT, 'Dalvik/2.1.0 (Linux; U; Android 5.1.1; SM-G900F Build/LMY48Y)');
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array(
			'Impact360AuthToken: ' . $token
		));

		$output = curl_exec($this->_curl);

		if($jsonDecode = json_decode($output, true))
			if(isset($jsonDecode['organization'][0]['id']))
				return $jsonDecode['organization'][0]['id'];
		return false;
	}

	public function getAgenda($token, $userinfo, $organisationId) {
		$startDate = date('Y-m-d\T23:00:00\Z', strtotime('-2 weeks'));
		$endDate = date('Y-m-d\T23:00:00\Z', strtotime('+4 weeks'));

		$this->renew();

		$data = array(
			'employeeId' => $userinfo['employee'][0]['id'],
			'organizationId' => $organisationId,
			'startTime' => $startDate,
			'endTime' => $endDate
		);

		curl_setopt($this->_curl, CURLOPT_URL, "https://wfo.kpn.com/wfo/rest/fs-api/schedule/get?" . http_build_query($data));
		curl_setopt($this->_curl, CURLOPT_USERAGENT, 'Dalvik/2.1.0 (Linux; U; Android 5.1.1; SM-G900F Build/LMY48Y)');
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array(
			'Impact360AuthToken: ' . $token
		));

		$output = curl_exec($this->_curl);


		if($jsonDecode = json_decode($output, true)) {
			return $this->agendaToiCall($jsonDecode, $userinfo);
		}

		return false;


	}

	private function agendaToiCall($data, $userinfo) {
		$dataToReturn = "BEGIN:VCALENDAR\r\n";
		$dataToReturn .= "VERSION:2.0\r\n";
		$dataToReturn .= "PRODID:-//Rooster door Dana Kool //rooster.kpn.com//\r\n";
        $dataToReturn .= "METHOD:PUBLISH\r\n";
        $dataToReturn .= "CALSCALE:GREGORIAN\r\n";
        $dataToReturn .= "X-WR-TIMEZONE:Europe/Amsterdam\r\n";
        $dataToReturn .= "X-WR-CALNAME:KPN Rooster voor " . $userinfo['employee'][0]['userInfo']['firstName'] . " " . $userinfo['employee'][0]['userInfo']['lastName'] . "\r\n";
        $dataToReturn .= "X-WR-CALDESC:KPN Rooster\r\n";
        $dataToReturn .= "REFRESH-INTERVAL;VALUE=DURATION:PT12H\r\n";
        $dataToReturn .= "X-PUBLISHED-TTL:PT12H\r\n";
        $dataToReturn .= "BEGIN:VTIMEZONE\r\n";
        $dataToReturn .= "TZID:Europe/Amsterdam\r\n";
        $dataToReturn .= "TZURL:http://tzurl.org/zoneinfo-outlook/Europe/Amsterdam\r\n";
        $dataToReturn .= "X-LIC-LOCATION:Europe/Amsterdam\r\n";
        $dataToReturn .= "BEGIN:DAYLIGHT\r\n";
        $dataToReturn .= "TZOFFSETFROM:+0100\r\n";
        $dataToReturn .= "TZOFFSETTO:+0200\r\n";
        $dataToReturn .= "TZNAME:CEST\r\n";
        $dataToReturn .= "DTSTART:19700329T020000\r\n";
        $dataToReturn .= "RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\r\n";
        $dataToReturn .= "END:DAYLIGHT\r\n";
        $dataToReturn .= "BEGIN:STANDARD\r\n";
        $dataToReturn .= "TZOFFSETFROM:+0200\r\n";
        $dataToReturn .= "TZOFFSETTO:+0100\r\n";
        $dataToReturn .= "TZNAME:CET\r\n";
        $dataToReturn .= "DTSTART:19701025T030000\r\n";
        $dataToReturn .= "RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\r\n";
        $dataToReturn .= "END:STANDARD\r\n";
        $dataToReturn .= "END:VTIMEZONE\r\n";

		$dateNow = date('Ymd\THis\z', time());

		$days = array();

		if(isset($data['scheduleEvents'])) {
			foreach($data['scheduleEvents'] as $index => $event) {
				if($event['activityId'] == 58)
					continue;

				if($this->byDay) {
					$day = date('Y-m-d', strtotime($event["startTime"]));
					if(!array_key_exists($day, $days)) {
						$days[$day] = array(
							'start' => strtotime($event["startTime"]),
							'end' => strtotime($event["endTime"])
						);
					} else {
						$dateData = $days[$day];

						if(strtotime($event["startTime"]) < $dateData['start'])
							$days[$day]['start'] = strtotime($event["startTime"]);

						if(strtotime($event["endTime"]) > $dateData['end'])
							$days[$day]['end'] = strtotime($event["endTime"]);
					}
				} else {

					$dataToReturn .= "BEGIN:VEVENT\r\n";
					$dataToReturn .= "DTSTAMP:" . $dateNow . "\r\n";
					$dataToReturn .= "DTSTART;TZID=Europe/Amsterdam:" . str_replace(array("-", ":"), "", $event["startTime"]) . "\r\n";
					$dataToReturn .= "DTEND;TZID=Europe/Amsterdam:" . str_replace(array("-", ":"), "", $event["endTime"]) . "\r\n";
					$dataToReturn .= "SUMMARY:" . $event['activityName'] . "\r\n";
					$dataToReturn .= "LOCATION:\r\n";
					$dataToReturn .= "TRANSP:OPAQUE\r\n";
					$dataToReturn .= "STATUS:CONFIRMED\r\n";
					$dataToReturn .= "SEQUENCE:0\r\n";
					$dataToReturn .= "CREATED:" . $dateNow . "\r\n";
					$dataToReturn .= "LAST-MODIFIED:" . $dateNow . "\r\n";
					$dataToReturn .= "UID:" . md5($index) . "\r\n";
					$dataToReturn .= "DESCRIPTION:\r\n";
					$dataToReturn .= "END:VEVENT\r\n";

				}
			}
			if($this->byDay) {

				foreach($days as $index => $day) {
					$dataToReturn .= "BEGIN:VEVENT\r\n";
					$dataToReturn .= "DTSTAMP:" . $dateNow . "\r\n";
					$dataToReturn .= "DTSTART;TZID=Europe/Amsterdam:" . date('Ymd\THis\Z', $day['start']) . "\r\n";
					$dataToReturn .= "DTEND;TZID=Europe/Amsterdam:" . date('Ymd\THis\Z', $day['end']) . "\r\n";
					$dataToReturn .= "SUMMARY:ARBEIT\r\n";
					$dataToReturn .= "LOCATION:\r\n";
					$dataToReturn .= "TRANSP:OPAQUE\r\n";
					$dataToReturn .= "STATUS:CONFIRMED\r\n";
					$dataToReturn .= "SEQUENCE:0\r\n";
					$dataToReturn .= "CREATED:" . $dateNow . "\r\n";
					$dataToReturn .= "LAST-MODIFIED:" . $dateNow . "\r\n";
					$dataToReturn .= "UID:" . md5($index) . "\r\n";
					$dataToReturn .= "DESCRIPTION:\r\n";
					$dataToReturn .= "END:VEVENT\r\n";
				}
			}
		}

		return $dataToReturn;
	}


	private function renew() {
		if($this->_curl)
			curl_close($this->_curl);
		$this->_curl = curl_init();
	}
}

if(!isset($_GET['key']))
	die('Invalid parameters');

$decrypt = Crypt::decrypt(Crypt::base64url_decode($_GET['key']));
$data = json_decode($decrypt, true);

if(!$data || ($data && !isset($data['username'])))
	die('Invalid key value');

$web = new Web();
//$token = $web->getToken('F13635', 'welkom01');
$token = $web->getToken('flore602', 'florobojo58');
//$token = $web->getToken($data['username'], $data['password']);
$userInfo = $web->getUserInfo($token);
$organisationId = $web->getOrganisationId($token);

header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: inline; filename=calendar.ics');
$web->byDay = (bool)($data['method'] == "1");

echo $web->getAgenda($token, $userInfo, $organisationId);
