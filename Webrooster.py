#!/bin/python3

from flask import Flask, make_response, render_template, request, jsonify

from AEScrypt import AEScrypt
from Rooster import Rooster, LoginError

cryptKey = "DWz78zD$.319Vmm"

app = Flask(__name__)


@app.route("/kpn/v2/<encrypted_string>")
def decrypt(encrypted_string):
    try:
        decrypted = AEScrypt(cryptKey).decrypt(encrypted_string)
        username, password = decrypted.split(',', 1)
        simple = True

        r = Rooster(username, password, simple)
        resp = make_response(r.ical, 200)
        resp.headers['Content-type'] = 'text/calendar; charset=utf-8'
        resp.headers['Content-Disposition'] = 'inline; filename=rooster.ics'
        return resp

    except LoginError as e:
        resp = render_template("403.html", error=e), 403
        return resp

@app.route("/kpn/v3/<int:simple>/<encrypted_string>")
def decryptv3(simple, encrypted_string):
    try:
        decrypted = AEScrypt(cryptKey).decrypt(encrypted_string)
        username, password = decrypted.split(',', 1)
        if simple == 1:
            simple = True
        else:
            simple = False

        r = Rooster(username, password, simple)
        resp = make_response(r.ical, 200)
        resp.headers['Content-type'] = 'text/calendar; charset=utf-8'
        resp.headers['Content-Disposition'] = 'inline; filename=rooster.ics'
        return resp

    except LoginError as e:
        resp = render_template("403.html", error=e), 403
        return resp


@app.route('/')
def index():
    return render_template('index.html', script=True), 200


@app.route('/robots.txt')
def robots():
    return 'User-agent: *\r\nDisallow: /\r\n'


@app.route('/encrypt', methods=["POST"])
def encrypt():
    username = request.form['username']
    password = request.form['password']

    crypted = AEScrypt(cryptKey).encrypt(username + "," + password)
    json = [{'crypt': crypted}]

    return jsonify(results=json)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', title="Pagina niet gevonden"), 404


@app.errorhandler(500)
def server_error(e):
    return render_template('500.html', title="Server error"), 500


if __name__ == "__main__":
    import logging

    logging.basicConfig(filename='agenda.log', level=logging.INFO)
    app.run(host='0.0.0.0')
